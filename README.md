# Shackle

An experiment in language learning and development.

The `index.html` file in this repo:

* Is self-contained, unstyled, and can be opened in the browser as `file:///path/to/shackle/index.html`, no server necessary.
* Exposes an HTML form for evaluating words in the Shackle language.
    * Numbers, strings, and arrays and objects of numbers and strings are interpreted as JSON and pushed onto the stack.
    * Symbols (non-numeral + anything else) are looked up in the word dictionary, which is empty to start.
    * If the word is not understood, you're prompted to define it.
* Stores the stack and the word dictionary in the URL search params, visible in your browser.

The next "missing piece" I plan to add is a mechanism for delayed evaluation, which will allow one to write conditionals and looping constructs.

## License

Copyright (c) 2020 Daniel Gregoire

Source code licensed under [Mozilla Public License Version 2.0](https://www.mozilla.org/en-US/MPL/2.0/)